use e_wardrobe;


select cloth_id, brand, cloth_type, photo, season_season_id, user_user_id from cloth_occasion co
join cloth cl on cl.cloth_id = co.cloth_list_occasion_cloth_id
join occasion occ on co.occasion_occasion_id = occ.occasion_id
join season sea on sea.season_id = cl.season_season_id
where cl.brand like "VANS" and sea.season_name like "ALL_YEAR" and cl.cloth_type like 1;