package pl.javagda25.ewardrobe.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.javagda25.ewardrobe.model.ClothType;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ClothTypeRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private ClothTypeRepository clothTypeRepository;

    @Test
    public void testSave() {
        ClothType savedClothType = testEntityManager.persist(getClothType());
        ClothType foundClothType = clothTypeRepository.getOne(savedClothType.getId());
        assertThat(foundClothType).isEqualTo(savedClothType);
    }

    @Test
    public void testFindAll() {
        testEntityManager.persist(getClothType());
        testEntityManager.persist(getClothType());

        List<ClothType> foundClothTypes = clothTypeRepository.findAll();
        List<ClothType> checkList = new ArrayList<>();
        for (ClothType clothType : foundClothTypes) {
            checkList.add(clothType);
        }
        assertThat(checkList.size()).isEqualTo(foundClothTypes.size());
    }

    @Test
    public void testExistByName() {
        ClothType savedClothType = testEntityManager.persist(getClothType());
        assertThat(clothTypeRepository.existsByName(savedClothType.getName())).isTrue();
    }

    @Test
    public void testUpdate() {
        String newClothType = "NewClothType";
        ClothType savedClothType = testEntityManager.persist(getClothType());
        clothTypeRepository.getOne(savedClothType.getId()).setName(newClothType);
        testEntityManager.persist(savedClothType);
        ClothType foundClothType = clothTypeRepository.getOne(savedClothType.getId());
        assertThat(foundClothType.getName()).isEqualTo(newClothType);
    }

    @Test
    public void testDelete() {
        ClothType savedClothType1 = testEntityManager.persist(getClothType());
        ClothType savedClothType2 = testEntityManager.persist(getClothType());
        clothTypeRepository.delete(savedClothType1);

        List<ClothType> foundClothTypes = clothTypeRepository.findAll();
        assertThat(foundClothTypes).containsOnly(savedClothType2);
    }

    private ClothType getClothType() {
        ClothType clothType = new ClothType();
        clothType.setName("HAT");
        return clothType;
    }
}
