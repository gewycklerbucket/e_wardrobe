package pl.javagda25.ewardrobe.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.javagda25.ewardrobe.model.Occasion;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class OccasionRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private OccasionRepository occasionRepository;

    @Test
    public void testSave() {
        Occasion savedOccasion = testEntityManager.persist(getOccasion());
        Occasion foundOccasion = occasionRepository.getOne(savedOccasion.getOccasionId());
        assertThat(foundOccasion).isEqualTo(savedOccasion);
    }

    @Test
    public void testFindAll() {
        testEntityManager.persist(getOccasion());
        testEntityManager.persist(getOccasion());
        List<Occasion> foundOccasions = occasionRepository.findAll();
        List<Occasion> checkList = new ArrayList<>();
        for (Occasion occasion : foundOccasions) {
            checkList.add(occasion);
        }
        assertThat(checkList.size()).isEqualTo(foundOccasions.size());
    }

    @Test
    public void testUpdate() {
        String newOccasionName = "new Occasion Name";
        Occasion savedOccasion = testEntityManager.persist(getOccasion());
        occasionRepository.getOne(savedOccasion.getOccasionId()).setOccasionName(newOccasionName);
        testEntityManager.persist(savedOccasion);
        Occasion foundOccasion = occasionRepository.getOne(savedOccasion.getOccasionId());
        assertThat(foundOccasion.getOccasionName()).isEqualTo(newOccasionName);
    }

    @Test
    public void testDeleteById() {
        Occasion savedOccasion1 = testEntityManager.persist(getOccasion());
        Occasion savedOccasion2 = testEntityManager.persist(getOccasion());
        occasionRepository.deleteById(savedOccasion1.getOccasionId());

        List<Occasion> foundOccasions = occasionRepository.findAll();
        assertThat(foundOccasions).containsOnly(savedOccasion2);
    }

    @Test
    public void testExistByOccasionName() {
        Occasion savedOccasion = testEntityManager.persist(getOccasion());
        assertThat(occasionRepository.existsByOccasionName(savedOccasion.getOccasionName())).isTrue();
    }

    private Occasion getOccasion() {
        Occasion occasion = new Occasion();
        occasion.setOccasionName("ALL DAY");
        return occasion;
    }
}
