package pl.javagda25.ewardrobe.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.javagda25.ewardrobe.model.Brand;
import pl.javagda25.ewardrobe.model.Cloth;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ClothRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;
    @Autowired
    private ClothRepository clothRepository;

    @Test
    public void testSave() {
        Cloth savedCloth = testEntityManager.persist(getCloth());
        Cloth foundCloth = clothRepository.getOne(savedCloth.getClothId());
        assertThat(foundCloth).isEqualTo(savedCloth);
    }

    @Test
    public void testFindById() {
        Cloth savedCloth = testEntityManager.persist(getCloth());
        Cloth foundCloth = clothRepository.findById(savedCloth.getClothId()).get();
        assertThat(foundCloth.getClothId()).isEqualTo(savedCloth.getClothId());
    }

    @Test
    public void testFindAll() {
        testEntityManager.persist(getCloth());
        testEntityManager.persist(getCloth());

        List<Cloth> clothList = clothRepository.findAll();
        List<Cloth> checkList = new ArrayList<>();
        for (Cloth cloth : clothList) {
            checkList.add(cloth);
        }
        assertThat(checkList.size()).isEqualTo(clothList.size());
    }

    @Test
    public void testUpdate() {
        String newClothName = "NewName";
        Cloth savedCloth = testEntityManager.persist(getCloth());
        clothRepository.getOne(savedCloth.getClothId()).getBrand().setBrandName(newClothName);
        testEntityManager.persist(savedCloth);
        Cloth foundCloth = clothRepository.getOne(savedCloth.getClothId());

        assertThat(foundCloth.getBrand().getBrandName()).isEqualTo(newClothName);
    }

    @Test
    public void testDeleteById() {
        Cloth savedCloth1 = testEntityManager.persist(getCloth());
        Cloth savedCloth2 = testEntityManager.persist(getCloth());
        clothRepository.deleteById(savedCloth1.getClothId());

        List<Cloth> clothList = clothRepository.findAll();
        assertThat(clothList.size()).isEqualTo(1);
        assertThat(clothList).containsOnly(savedCloth2);
    }

    private Cloth getCloth() {
        Brand brand = new Brand();
        brand.setBrandName("testBrand");
        testEntityManager.persist(brand);

        Cloth cloth = new Cloth();
        cloth.setBrand(brand);
        return cloth;
    }
}