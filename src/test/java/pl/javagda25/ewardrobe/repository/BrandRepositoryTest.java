package pl.javagda25.ewardrobe.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.javagda25.ewardrobe.model.Brand;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BrandRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private BrandRepository brandRepository;

    @Test
    public void testSave() {
        Brand savedBrand = testEntityManager.persist(getBrand());
        Brand foundBrand = brandRepository.getOne(savedBrand.getBrandId());
        assertThat(foundBrand).isEqualTo(savedBrand);
    }

    @Test
    public void testFindAll() {
        testEntityManager.persist(getBrand());
        testEntityManager.persist(getBrand());

        List<Brand> brandFromDb = brandRepository.findAll();
        List<Brand> checkList = new ArrayList<>();
        for (Brand brand : brandFromDb) {
            checkList.add(brand);
        }
        assertThat(checkList.size()).isEqualTo(brandFromDb.size());
    }

    //UPDATE
    //DELETE


    @Test
    public void testExistByName() {
        Brand savedBrand = testEntityManager.persist(getBrand());
        assertThat(brandRepository.existsByBrandName(savedBrand.getBrandName())).isTrue();
    }

    private Brand getBrand() {
        Brand brand = new Brand();
        brand.setBrandName("testBrand");
        return brand;
    }
}
