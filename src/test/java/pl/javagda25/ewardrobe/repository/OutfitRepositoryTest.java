package pl.javagda25.ewardrobe.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.javagda25.ewardrobe.model.Outfit;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class OutfitRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private OutfitRepository outfitRepository;

    @Test
    public void testSave() {
        Outfit savedOutfit = testEntityManager.persist(getOutfit());
        Outfit foundOutfit = outfitRepository.getOne(savedOutfit.getOutfitId());
        assertThat(foundOutfit).isEqualTo(savedOutfit);
    }

    @Test
    public void testFindAll() {
        testEntityManager.persist(getOutfit());
        testEntityManager.persist(getOutfit());

        List<Outfit> foundOutfits = outfitRepository.findAll();
        List<Outfit> checkList = new ArrayList<>();
        for (Outfit outfit : foundOutfits) {
            checkList.add(outfit);
        }
        assertThat(checkList.size()).isEqualTo(foundOutfits.size());
    }

    @Test
    public void testFindById() {
        Outfit savedOutfit = testEntityManager.persist(getOutfit());
        Optional<Outfit> foundOutfit = outfitRepository.findById(savedOutfit.getOutfitId());
        assertThat(foundOutfit.isPresent()).isEqualTo(savedOutfit);
    }

    @Test
    public void testUpdate() {
        String newOutfitName = "New Outfit Name";
        Outfit savedOutfit = testEntityManager.persist(getOutfit());
        outfitRepository.getOne(savedOutfit.getOutfitId()).setName(newOutfitName);
        testEntityManager.persist(savedOutfit);
        Outfit foundOutfit = outfitRepository.getOne(savedOutfit.getOutfitId());
        assertThat(foundOutfit.getName()).isEqualTo(newOutfitName);
    }

    @Test
    public void testDeleteById() {
        Outfit savedOutfit1 = testEntityManager.persist(getOutfit());
        Outfit savedOutfit2 = testEntityManager.persist(getOutfit());

        outfitRepository.deleteById(savedOutfit1.getOutfitId());

        List<Outfit> foundOutfit = outfitRepository.findAll();
        assertThat(foundOutfit).containsOnly(savedOutfit2);
    }

    private Outfit getOutfit() {
        Outfit outfit = new Outfit();
        outfit.setName("SWAGFIT");
        return outfit;
    }
}
