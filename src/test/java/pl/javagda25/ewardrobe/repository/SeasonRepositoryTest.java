package pl.javagda25.ewardrobe.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.javagda25.ewardrobe.model.Season;
import pl.javagda25.ewardrobe.model.enums.SeasonName;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class SeasonRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private SeasonRepository seasonRepository;

    @Test
    public void testSave() {
        Season savedSeason = testEntityManager.persist(getSeason());
        Season foundSeason = seasonRepository.getOne(savedSeason.getSeasonId());
        assertThat(foundSeason).isEqualTo(savedSeason);
    }

    @Test
    public void testFindAll() {
        testEntityManager.persist(getSeason());
        testEntityManager.persist(getSeason());

        List<Season> foundSeasons = seasonRepository.findAll();
        List<Season> checkList = new ArrayList<>();
        for (Season season : foundSeasons) {
            checkList.add(season);
        }
        assertThat(checkList.size()).isEqualTo(foundSeasons.size());
    }

    @Test
    public void testUpdate() {
        SeasonName newSeasonName = SeasonName.valueOf("new Season");
        Season savedSeason = testEntityManager.persist(getSeason());
        seasonRepository.getOne(savedSeason.getSeasonId()).setSeasonName(newSeasonName);
        testEntityManager.persist(savedSeason);
        Season foundSeason = seasonRepository.getOne(savedSeason.getSeasonId());
        assertThat(foundSeason.getSeasonName()).isEqualTo(newSeasonName);
    }

    @Test
    public void testDelete() {
        Season savedSeasoon1 = testEntityManager.persist(getSeason());
        Season savedSeasoon2 = testEntityManager.persist(getSeason());

        seasonRepository.delete(savedSeasoon1);
        List<Season> foundSeasons = seasonRepository.findAll();

        assertThat(foundSeasons).containsOnly(savedSeasoon2);
    }

    @Test
    public void testExistByOccasionName() {
        Season savedSeason = testEntityManager.persist(getSeason());
        assertThat(seasonRepository.existsBySeasonName(savedSeason.getSeasonName())).isTrue();
    }

    private Season getSeason() {
        Season season = new Season();
        season.setSeasonName(SeasonName.ALL_YEAR);
        return season;
    }
}
