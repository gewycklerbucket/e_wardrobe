package pl.javagda25.ewardrobe.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.javagda25.ewardrobe.model.ClothType;
import pl.javagda25.ewardrobe.repository.ClothTypeRepository;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ClothTypeServiceTest {

    private ClothType clothType = new ClothType();

    @InjectMocks
    private ClothTypeService clothTypeService;

    @Mock
    private ClothTypeRepository clothTypeRepository;

    @Before
    public void setUp() {
        clothType.setId(1L);
        clothType.setName("name");
    }

    @Test
    public void addClothType() {
        clothTypeService.addClothType(clothType);
        ArgumentCaptor<ClothType> arg = ArgumentCaptor.forClass(ClothType.class);
        Mockito.verify(clothTypeRepository, Mockito.times(1)).save(arg.capture());
        assertThat("NAME").isEqualTo(arg.getValue().getName());
        assertThat("name").isNotEqualTo(arg.getValue().getName());
    }

    @Test
    public void getAll() {
        List<ClothType> clothTypeList = Arrays.asList(clothType);
        Mockito.when(clothTypeRepository.findAll()).thenReturn(clothTypeList);

        assertThat(clothTypeService.getAll().size()).isEqualTo(clothTypeList.size());
        assertThat(clothTypeService.getAll()).isInstanceOf(List.class);

        Mockito.verify(clothTypeRepository, Mockito.times(2)).findAll();
    }
}