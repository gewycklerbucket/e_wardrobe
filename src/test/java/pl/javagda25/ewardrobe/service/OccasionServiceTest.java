package pl.javagda25.ewardrobe.service;

import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.javagda25.ewardrobe.model.Occasion;
import pl.javagda25.ewardrobe.repository.OccasionRepository;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class OccasionServiceTest {

    private Occasion occasion = new Occasion();

    @InjectMocks
    private OccasionService occasionService;

    @Mock
    private OccasionRepository occasionRepository;

    @Before
    public void setUpt() {
        occasion.setOccasionId(1L);
        occasion.setOccasionName("name");
    }

    @Test
    public void findAll() {
        List<Occasion> occasons = Arrays.asList(occasion);
        Mockito.when(occasionRepository.findAll()).thenReturn(occasons);

        assertThat(occasionService.findAll()).isInstanceOf(List.class);
        AssertionsForClassTypes.assertThat(occasionService.findAll().size()).isEqualTo(occasons.size());

        Mockito.verify(occasionRepository, Mockito.times(2)).findAll();
    }

    @Test
    public void addOccasion() {
        occasionService.addOccasion(occasion);

        ArgumentCaptor<Occasion> arg = ArgumentCaptor.forClass(Occasion.class);

        Mockito.verify(occasionRepository, Mockito.times(1)).save(arg.capture());

        assertThat("name").isNotEqualTo(arg.getValue().getOccasionName());
        assertThat("NAME").isEqualTo(arg.getValue().getOccasionName());
    }
}