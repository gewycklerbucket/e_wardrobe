package pl.javagda25.ewardrobe.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.javagda25.ewardrobe.model.Season;
import pl.javagda25.ewardrobe.model.enums.SeasonName;
import pl.javagda25.ewardrobe.repository.SeasonRepository;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class SeasonServiceTest {

    private Season season = new Season();

    @InjectMocks
    private SeasonService seasonService;

    @Mock
    private SeasonRepository seasonRepository;

    @Before
    public void setUp() {
        season.setSeasonName(SeasonName.ALL_YEAR);
        season.setSeasonId(1L);
    }

    @Test
    public void findAll() {
        List<Season> seasonList = Arrays.asList(season);

        Mockito.when(seasonRepository.findAll()).thenReturn(seasonList);
        assertThat(seasonService.findAll().size()).isEqualTo(seasonList.size());
        assertThat(seasonService.findAll()).isInstanceOf(List.class);

        Mockito.verify(seasonRepository, Mockito.times(2)).findAll();
    }
}