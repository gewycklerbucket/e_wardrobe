package pl.javagda25.ewardrobe.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.javagda25.ewardrobe.model.Cloth;
import pl.javagda25.ewardrobe.model.ClothType;
import pl.javagda25.ewardrobe.model.Outfit;
import pl.javagda25.ewardrobe.repository.ClothRepository;
import pl.javagda25.ewardrobe.repository.OutfitRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(MockitoJUnitRunner.class)
public class OutfitServiceTest {

    private Outfit outfit = new Outfit();

    @InjectMocks
    private OutfitService outfitService;

    @Mock
    private OutfitRepository outfitRepository;

    @Mock
    private ClothRepository clothRepository;

    @Before
    public void setUp() {
        outfit.setName("name");
        outfit.setOutfitId(1L);
    }

    @Test
    public void save() {
        ArgumentCaptor<Outfit> argument = ArgumentCaptor.forClass(Outfit.class);

        outfitService.save(outfit);

        Mockito.verify(outfitRepository, Mockito.times(1)).save(argument.capture());

        assertThat("name").isEqualTo(argument.getValue().getName());
    }

    @Test
    public void shouldAddClothToOutfit() {
        Cloth cloth1 = getCloth(1L);

        Mockito.when(outfitRepository.findById(outfit.getOutfitId())).thenReturn(Optional.of(outfit));
        Mockito.when(clothRepository.findById(cloth1.getClothId())).thenReturn(Optional.of(cloth1));

        outfitService.addClothToOutfit(outfit.getOutfitId(), cloth1.getClothId());

        assertThat(outfit.getClothSet()).contains(cloth1);
    }

    @Test
    public void shouldReplaceExistingClothOfTheSameTypeInOutfit() {
        Cloth cloth1 = getCloth(1L);
        Cloth cloth2 = getCloth(2L);

        outfit.getClothSet().add(cloth1);

        Mockito.when(outfitRepository.findById(outfit.getOutfitId())).thenReturn(Optional.of(outfit));
        Mockito.when(clothRepository.findById(cloth2.getClothId())).thenReturn(Optional.of(cloth2));

        outfitService.addClothToOutfit(outfit.getOutfitId(), cloth2.getClothId());

        assertThat(outfit.getClothSet()).contains(cloth2);
        assertThat(outfit.getClothSet()).doesNotContain(cloth1);
    }

    @Test
    public void shouldFindById() {
        Mockito.when(outfitRepository.findById(outfit.getOutfitId())).thenReturn(Optional.of(outfit));

        assertThat(outfitService.findById(outfit.getOutfitId())).isEqualTo(outfit);
    }

    @Test
    public void shouldNotFindByIdAndThrowException() {
        Mockito.when(outfitRepository.findById(outfit.getOutfitId())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> outfitService.findById(outfit.getOutfitId()));
    }

    @Test
    public void deleteIfNull_shouldDeleteOutfitWithEmptyClothSet() {
        Cloth cloth = getCloth(1L);
        outfit.getClothSet().add(cloth);

        Mockito.when(outfitRepository.findAll()).thenReturn(Arrays.asList(outfit));
        outfitService.deleteIfNull();
        Mockito.verify(outfitRepository, Mockito.times(0)).deleteById(outfit.getOutfitId());
    }

    @Test
    public void deleteIfNull_shouldNotDeleteOutfitWithEmmptyClothSet() {
        Mockito.when(outfitRepository.findAll()).thenReturn(Arrays.asList(outfit));
        outfitService.deleteIfNull();
        Mockito.verify(outfitRepository, Mockito.times(1)).deleteById(outfit.getOutfitId());
    }

    @Test
    public void removeClothFromOutfit() {
        Cloth cloth = getCloth(1L);
        outfit.getClothSet().add(cloth);

        Mockito.when(outfitRepository.findById(outfit.getOutfitId())).thenReturn(Optional.of(outfit));
        Mockito.when(clothRepository.findById(outfit.getOutfitId())).thenReturn(Optional.of(cloth));

        outfitService.removeClothFromOutfit(outfit.getOutfitId(), cloth.getClothId());

        Mockito.verify(outfitRepository, Mockito.times(1)).save(outfit);
        assertThat(outfit.getClothSet()).doesNotContain(cloth);
    }

    @Test
    public void findAll() {
        List<Outfit> outfits = Arrays.asList(outfit);
        Mockito.when(outfitRepository.findAll()).thenReturn(outfits);

        List<Outfit> foundOutfits = outfitService.findAll();

        assertThat(foundOutfits).isInstanceOf(List.class);
        assertThat(foundOutfits.size()).isEqualTo(outfits.size());

        Mockito.verify(outfitRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void deleteById() {
        outfitService.deleteById(outfit.getOutfitId());

        Mockito.verify(outfitRepository, Mockito.times(1)).deleteById(outfit.getOutfitId());
    }

    private Cloth getCloth(Long id) {
        Cloth cloth = new Cloth();
        cloth.setClothId(id);
        cloth.setClothType(getClothType());
        return cloth;
    }

    private ClothType getClothType() {
        ClothType clothType = new ClothType();
        clothType.setName("HAT");
        return clothType;
    }
}