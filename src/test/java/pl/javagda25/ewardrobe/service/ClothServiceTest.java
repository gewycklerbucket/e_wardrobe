package pl.javagda25.ewardrobe.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import pl.javagda25.ewardrobe.model.*;
import pl.javagda25.ewardrobe.model.enums.SeasonName;
import pl.javagda25.ewardrobe.repository.*;

import javax.persistence.EntityNotFoundException;
import java.util.*;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ClothServiceTest {

    private Cloth cloth = new Cloth();

    @InjectMocks
    private ClothService clothService;

    @Mock
    private ClothRepository clothRepository;

    @Mock
    private ClothTypeRepository clothTypeRepository;

    @Mock
    private SeasonRepository seasonRepository;

    @Mock
    private OccasionRepository occasionRepository;

    @Mock
    private BrandRepository brandRepository;

    @Mock(answer = Answers.RETURNS_SMART_NULLS)
    private MultipartFile emptyFile;

    private MockMultipartFile filledFile = new MockMultipartFile("name", new byte[8]);

    @Before
    public void setUp() {
        cloth.setClothId(1L);
    }

    @Test
    public void addCloth_shouldSaveClothWithPhoto() {
        mockMethodFromOtherRepositiories();
        clothService.addCloth(cloth, 1L, 1L, filledFile, 1L, 1L);

        ArgumentCaptor<Cloth> clothArg = ArgumentCaptor.forClass(Cloth.class);

        Mockito.verify(clothRepository, Mockito.times(1)).save(clothArg.capture());

        assertThat(cloth).isInstanceOf(Cloth.class);
        assertThat(cloth.getClothId()).isEqualTo(clothArg.getValue().getClothId());
        assertThat(cloth.getBrand()).isEqualTo(clothArg.getValue().getBrand());
        assertThat(cloth.getPhoto()).isEqualTo(clothArg.getValue().getPhoto());
        assertThat(cloth.getClothType()).isEqualTo(clothArg.getValue().getClothType());
        assertThat(cloth.getOccasion()).isEqualTo(clothArg.getValue().getOccasion());
        assertThat(cloth.getSeason()).isEqualTo(clothArg.getValue().getSeason());
    }

    @Test
    public void addCloth_shouldSaveClothWithoutPhoto() {
        mockMethodFromOtherRepositiories();
        clothService.addCloth(cloth, 1L, 1L, emptyFile, 1L, 1L);
        ArgumentCaptor<Cloth> clothArg = ArgumentCaptor.forClass(Cloth.class);

        Mockito.verify(clothRepository, Mockito.times(1)).save(clothArg.capture());

        assertThat(cloth).isInstanceOf(Cloth.class);
        assertThat(cloth.getClothId()).isEqualTo(clothArg.getValue().getClothId());
        assertThat(cloth.getBrand()).isEqualTo(clothArg.getValue().getBrand());
        assertThat(cloth.getPhoto()).isEmpty();
        assertThat(cloth.getClothType()).isEqualTo(clothArg.getValue().getClothType());
        assertThat(cloth.getOccasion()).isEqualTo(clothArg.getValue().getOccasion());
        assertThat(cloth.getSeason()).isEqualTo(clothArg.getValue().getSeason());
    }

    @Test
    public void addCloth_shouldReplaceOldPhotoWithNewOne() {
        mockMethodFromOtherRepositiories();
        byte[] photo = new byte[0];
        cloth.setPhoto(photo);
        clothService.addCloth(cloth, 1L, 1L, filledFile, 1L, 1L);

        ArgumentCaptor<Cloth> clothArg = ArgumentCaptor.forClass(Cloth.class);

        Mockito.verify(clothRepository, Mockito.times(1)).save(clothArg.capture());

        assertThat(cloth).isInstanceOf(Cloth.class);
        assertThat(cloth.getClothId()).isEqualTo(clothArg.getValue().getClothId());
        assertThat(cloth.getBrand()).isEqualTo(clothArg.getValue().getBrand());
        assertThat(cloth.getPhoto()).isNotEmpty();
        assertThat(cloth.getPhoto()).isNotEqualTo(photo);
        assertThat(cloth.getClothType()).isEqualTo(clothArg.getValue().getClothType());
        assertThat(cloth.getOccasion()).isEqualTo(clothArg.getValue().getOccasion());
        assertThat(cloth.getSeason()).isEqualTo(clothArg.getValue().getSeason());
    }

    @Test(expected = EntityNotFoundException.class)
    public void addCloth_shouldThrowEntityNotFoundExceptionCausedByMissingValue() {
        clothService.addCloth(cloth, 1L, 1L, emptyFile, 1L, 1L);
    }

    @Test
    public void getAll_shouldReturnAllCloth() {
        Mockito.when(clothRepository.findAll()).thenReturn(Arrays.asList(cloth));

        List<Cloth> foundClothList = clothService.getAll(null, null, null, null);
        assertThat(foundClothList).isNotEmpty();
        assertThat(foundClothList).contains(cloth);
    }

    @Test
    public void getAll_shouldReturnAllClothWithGivenBrandId() {
        Brand brand = new Brand(2L, Arrays.asList(cloth), "name");
        Mockito.when(brandRepository.getOne(brand.getBrandId())).thenReturn(brand);

        List<Cloth> foundClothList = clothService
                .getAll(brand.getBrandId(), null, null, null);

        assertThat(foundClothList).isNotEmpty();
        assertThat(foundClothList).contains(cloth);
    }

    @Test
    public void getAll_shouldReturnAllClothWithGivenClothTypeId() {
        ClothType clothType = new ClothType(1L, "name", Arrays.asList(cloth));
        Mockito.when(clothTypeRepository.getOne(clothType.getId())).thenReturn(clothType);

        List<Cloth> foundClothList = clothService
                .getAll(null, clothType.getId(), null, null);

        assertThat(foundClothList).isNotEmpty();
        assertThat(foundClothList).contains(cloth);
    }

    @Test
    public void getAll_shouldReturnAllClothWithGivenOccasionId() {
        Set<Cloth> clothSet = new HashSet<>();
        clothSet.add(cloth);
        Occasion occasion = new Occasion(1L, "name", clothSet);
        Mockito.when(occasionRepository.getOne(occasion.getOccasionId())).thenReturn(occasion);

        List<Cloth> foundClothList = clothService
                .getAll(null, null, occasion.getOccasionId(), null);

        assertThat(foundClothList).isNotEmpty();
        assertThat(foundClothList).contains(cloth);
    }

    @Test
    public void getAll_shouldReturnAllClothWithGivenSeasonId() {
        Season season = new Season(1L, SeasonName.ALL_YEAR, Arrays.asList(cloth));
        Mockito.when(seasonRepository.getOne(season.getSeasonId())).thenReturn(season);

        List<Cloth> foundClothList = clothService
                .getAll(null, null, null, season.getSeasonId());

        assertThat(foundClothList).isNotEmpty();
        assertThat(foundClothList).contains(cloth);
    }

    @Test
    public void getById() {
        Mockito.when(clothRepository.findById(cloth.getClothId())).thenReturn(Optional.of(cloth));

        Cloth foundCloth = clothService.getById(cloth.getClothId()).get();

        assertThat(foundCloth).isNotNull();
        assertThat(foundCloth).isInstanceOf(Cloth.class);
        assertThat(foundCloth).isEqualTo(cloth);

        Mockito.verify(clothRepository, Mockito.times(1)).findById(cloth.getClothId());
    }

    @Test
    public void deleteById() {
        clothService.deleteById(cloth.getClothId());
        Mockito.verify(clothRepository, Mockito.times(1)).deleteById(cloth.getClothId());
    }

    @Test
    public void getAllNoFilter() {
        List<Cloth> clothList = Arrays.asList(cloth);
        Mockito.when(clothRepository.findAll()).thenReturn(clothList);

        List<Cloth> allFound = clothService.getAllNoFilter();

        assertThat(allFound).isInstanceOf(List.class);
        assertThat(allFound.size()).isEqualTo(clothList.size());
        assertThat(allFound).contains(cloth);

        Mockito.verify(clothRepository, Mockito.times(1)).findAll();
    }

    private void mockMethodFromOtherRepositiories() {
        Mockito.when(clothTypeRepository.findById(1L))
                .thenReturn(Optional.of(new ClothType(1L, "", new ArrayList<>())));

        Mockito.when(seasonRepository.findById(1L))
                .thenReturn(Optional.of(new Season(1L, SeasonName.ALL_YEAR, new ArrayList<>())));

        Mockito.when(brandRepository.findById(1L))
                .thenReturn(Optional.of(new Brand(1L, new ArrayList<>(), "")));

        Mockito.when(occasionRepository.findById(1L))
                .thenReturn(Optional.of(new Occasion(1L, "", new HashSet<>())));
    }
}