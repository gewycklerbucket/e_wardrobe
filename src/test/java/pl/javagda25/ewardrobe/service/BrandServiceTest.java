package pl.javagda25.ewardrobe.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.javagda25.ewardrobe.model.Brand;
import pl.javagda25.ewardrobe.repository.BrandRepository;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class BrandServiceTest {

    @InjectMocks
    private BrandService brandService;

    @Mock
    private BrandRepository brandRepository;

    @Test
    public void getAll() {
        List<Brand> brandListToSave = preperedMockData();

        Mockito.when(brandRepository.findAll()).thenReturn(brandListToSave);
        assertThat(brandService.getAll().size()).isEqualTo(brandListToSave.size());

        Mockito.verify(brandRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void addBrand() {
        brandService.addBrand(getBrand(1L, "name"));

        ArgumentCaptor<Brand> argument = ArgumentCaptor.forClass(Brand.class);

        Mockito.verify(brandRepository, Mockito.times(1)).save(argument.capture());

        assertThat("name").isNotEqualTo(argument.getValue().getBrandName());
        assertThat("NAME").isEqualTo(argument.getValue().getBrandName());
    }

    private List<Brand> preperedMockData() {
        List<Brand> brandList = new ArrayList<>();
        brandList.add(getBrand(1L, "CCC"));
        brandList.add(getBrand(2L, "NIKE"));
        brandList.add(getBrand(3L, "PUMA"));
        return brandList;
    }

    private Brand getBrand(Long id, String name) {
        Brand brand = new Brand();
        brand.setBrandId(id);
        brand.setBrandName(name);
        return brand;
    }
}