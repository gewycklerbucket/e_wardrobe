package pl.javagda25.ewardrobe.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class BasicConfig implements WebMvcConfigurer {


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/static/images/**")
                .addResourceLocations("file:D:\\java\\projects\\projekt koncowy\\ewardrobe\\src\\main\\resources\\static\\images\\");
    }
}
