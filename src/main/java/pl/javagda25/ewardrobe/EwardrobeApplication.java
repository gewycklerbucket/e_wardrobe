package pl.javagda25.ewardrobe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EwardrobeApplication {

    public static void main(String[] args) {
        SpringApplication.run(EwardrobeApplication.class, args);

        // Zdjęcia muszą się konwertować przed zapisaem bo za długo trwa ich załadowanie do widoku strony

        // Przyciki mogły by się wyświetlać zawsze na dole modułu

    }

}
