package pl.javagda25.ewardrobe.model.enums;

public enum BrandNames {
    NO_NAME,
    ADIDAS,
    CCC,
    CiA,
    CONVERS,
    HiM,
    NEW_BALANCE,
    NIKE,
    PUMA,
    REEBOK,
    SUPREME,
    WRANGLER,
    VANS,
    ZARA;
}
