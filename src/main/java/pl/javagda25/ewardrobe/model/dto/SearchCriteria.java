package pl.javagda25.ewardrobe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import pl.javagda25.ewardrobe.model.Brand;
import pl.javagda25.ewardrobe.model.ClothType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchCriteria {
    Long brandId;
    Long clothTypeId;
    Long occasionId;
    Long seasonId;
}
