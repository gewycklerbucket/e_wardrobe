package pl.javagda25.ewardrobe.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.javagda25.ewardrobe.model.enums.SeasonName;
import pl.javagda25.ewardrobe.singleton.SeasonCheckerSingleton;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Outfit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long outfitId;

    private String name;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Cloth> clothSet = new HashSet<>();

    private SeasonName seasonName;

    public boolean checkIfContains(ClothType givenType) {
        return clothSet.stream().anyMatch(cloth -> cloth.getClothType().equals(givenType));
    }

    public Optional<Cloth> getClothOfGivenType(ClothType givenType) {
        return clothSet.stream().filter(cloth -> cloth.getClothType().equals(givenType)).findFirst();
    }

    public Boolean inSeason() {
        return SeasonCheckerSingleton.getInstance().inSeason(seasonName);
    }

    public void setTheSeason() {
        if (!clothSet.isEmpty()) {
            if (checkForSpringCloth()) {
                seasonName = SeasonName.SPRING;
                return;
            }
            if (checkForSummerCloth()) {
                seasonName = SeasonName.SUMMER;
                return;
            }
            if (checkForAutumnCloth()) {
                seasonName = SeasonName.AUTUMN;
                return;
            }
            if (checkForWinterCloth()) {
                seasonName = SeasonName.WINTER;
                return;
            }
            seasonName = SeasonName.ALL_YEAR;
        }
    }

    private boolean checkForSpringCloth() {
        return clothSet.stream()
                .noneMatch(c -> c.getSeason().getSeasonName().equals(SeasonName.WINTER) ||
                        c.getSeason().getSeasonName().equals(SeasonName.AUTUMN) ||
                        c.getSeason().getSeasonName().equals(SeasonName.AUTUMN_WINTER));

//                        anyMatch(cloth -> !cloth.getSeason().getSeasonName().equals(SeasonName.SPRING) ||
//                        !cloth.getSeason().getSeasonName().equals(SeasonName.SPRING_SUMMER));
    }

    private boolean checkForSummerCloth() {
        return clothSet.stream()
                .noneMatch(c -> c.getSeason().getSeasonName().equals(SeasonName.WINTER) ||
                        c.getSeason().getSeasonName().equals(SeasonName.SPRING) ||
                        c.getSeason().getSeasonName().equals(SeasonName.AUTUMN) ||
                        c.getSeason().getSeasonName().equals(SeasonName.AUTUMN_WINTER));

//                .anyMatch(cloth -> !cloth.getSeason().getSeasonName().equals(SeasonName.SUMMER) ||
//                        !cloth.getSeason().getSeasonName().equals(SeasonName.SPRING_SUMMER));
    }

    private boolean checkForAutumnCloth() {
        return clothSet.stream()
                .anyMatch(cloth -> cloth.getSeason().getSeasonName().equals(SeasonName.AUTUMN) ||
                        cloth.getSeason().getSeasonName().equals(SeasonName.AUTUMN_WINTER));
    }

    private boolean checkForWinterCloth() {
        return clothSet.stream()
                .anyMatch(cloth -> cloth.getSeason().getSeasonName().equals(SeasonName.WINTER) ||
                        cloth.getSeason().getSeasonName().equals(SeasonName.AUTUMN_WINTER));
    }
}
// naprawić sprawdzanie sezonowości outfitu
