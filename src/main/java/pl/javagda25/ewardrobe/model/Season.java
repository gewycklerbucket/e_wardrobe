package pl.javagda25.ewardrobe.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.javagda25.ewardrobe.model.enums.SeasonName;
import pl.javagda25.ewardrobe.singleton.SeasonCheckerSingleton;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Season {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long seasonId;

    @Enumerated(EnumType.STRING)
    private SeasonName seasonName;

    @OneToMany(mappedBy = "season", fetch = FetchType.EAGER)
    private List<Cloth> clothListSeason;

    public Season(SeasonName seasonName) {
        this.seasonName = seasonName;
    }

    public boolean inSeason() {
        return SeasonCheckerSingleton.getInstance().inSeason(seasonName);
    }

}
