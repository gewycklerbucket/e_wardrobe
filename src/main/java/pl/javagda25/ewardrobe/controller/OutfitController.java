package pl.javagda25.ewardrobe.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.javagda25.ewardrobe.model.Cloth;
import pl.javagda25.ewardrobe.model.Outfit;
import pl.javagda25.ewardrobe.service.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@AllArgsConstructor
@Controller
@RequestMapping("/outfit/")
public class OutfitController {
    private final OutfitService outfitService;
    private final ClothService clothService;
    private final OccasionService occasionService;
    private final SeasonService seasonService;
    private final BrandService brandService;
    private final ClothTypeService clothTypeService;

    @GetMapping("/addCloth")
    public String addClothToOutfit(Model model,
                                   @RequestParam(name = "outfitId") Long outfitId,
                                   @RequestParam(name = "clothId") Long clothId) {

        outfitService.addClothToOutfit(outfitId, clothId);
        Outfit outfit = outfitService.findById(outfitId);
//        sendBrandsClothTypesOccasionsSeasons(model);
        model.addAttribute("outfit", outfit);

        model.addAttribute("clothList", clothService.getAllNoFilter());

        model.addAttribute("brands", brandService.getAll());
        model.addAttribute("clothTypes", clothTypeService.getAll());
        model.addAttribute("occasionList", occasionService.findAll());
        model.addAttribute("seasonList", seasonService.findAll());

        return "outfit-add";
    }

    @GetMapping("/add")
    public String createOutfit(Model model, Outfit outfit, HttpServletRequest request) {
        outfitService.deleteIfNull();
//        sendBrandsClothTypesOccasionsSeasons(model);
        model.addAttribute("outfit", outfit);
        model.addAttribute("backReferer", request.getHeader("referer"));
        model.addAttribute("clothList", clothService.getAllNoFilter());

        model.addAttribute("brands", brandService.getAll());
        model.addAttribute("clothTypes", clothTypeService.getAll());
        model.addAttribute("occasionList", occasionService.findAll());
        model.addAttribute("seasonList", seasonService.findAll());


        outfitService.save(outfit);
        return "outfit-add";
    }

    @PostMapping("/add")
    public String createOutfit(Outfit outfit) {

        outfitService.save(outfit);
        return "redirect:/outfit/listOutfit";
    }

    @GetMapping("/update/{outfitId}")
    public String updateOutfit(Model model, HttpServletRequest request,
                               @PathVariable(name = "outfitId") Long outfitId) {

//        sendBrandsClothTypesOccasionsSeasons(model);
//        sendOutfitBackRefererClothList(model, outfitId, request);

        Outfit outfit = outfitService.findById(outfitId);
        model.addAttribute("outfit", outfit);
        model.addAttribute("backReferer", request.getHeader("referer"));
        model.addAttribute("clothList", clothService.getAllNoFilter());

        model.addAttribute("brands", brandService.getAll());
        model.addAttribute("clothTypes", clothTypeService.getAll());
        model.addAttribute("occasionList", occasionService.findAll());
        model.addAttribute("seasonList", seasonService.findAll());

        return "outfit-add";
    }

    @GetMapping("/listCloth")
    public String list(Model model,
                       @RequestParam(name = "outfitId") Long outfitId,
                       @RequestParam(name = "brandsFilter", required = false) Long brandId,
                       @RequestParam(name = "typeFilter", required = false) Long clothTypeId,
                       @RequestParam(name = "occasionFilter", required = false) Long occasionId,
                       @RequestParam(name = "seasonFilter", required = false) Long seasonId) {

        List<Cloth> clothList = clothService.getAll(brandId, clothTypeId, occasionId, seasonId);

//        sendBrandsClothTypesOccasionsSeasons(model);
        Outfit outfit = outfitService.findById(outfitId);
        model.addAttribute("outfit", outfit);
        model.addAttribute("clothList", clothList);

        model.addAttribute("brands", brandService.getAll());
        model.addAttribute("clothTypes", clothTypeService.getAll());
        model.addAttribute("occasionList", occasionService.findAll());
        model.addAttribute("seasonList", seasonService.findAll());

        return "outfit-add";
    }

    @GetMapping("/listOutfit")
    public String listOutfit(Model model, HttpServletRequest request) {
        outfitService.deleteIfNull();
        List<Outfit> outfitList = outfitService.findAll();
        model.addAttribute("outfitList", outfitList);
        model.addAttribute("backReferer", request.getHeader("referer"));
        return "outfit-list";
    }

    @GetMapping("/removeCloth")
    public String removeClothFromOutfit(Model model, HttpServletRequest request,
                                        @RequestParam(name = "outfitId") Long outfitId,
                                        @RequestParam(name = "clothId") Long clothId) {
        outfitService.removeClothFromOutfit(outfitId, clothId);

//        sendBrandsClothTypesOccasionsSeasons(model);
//        sendOutfitBackRefererClothList(model, outfitId, request);

        model.addAttribute("brands", brandService.getAll());
        model.addAttribute("clothTypes", clothTypeService.getAll());
        model.addAttribute("occasionList", occasionService.findAll());
        model.addAttribute("seasonList", seasonService.findAll());

        Outfit outfit = outfitService.findById(outfitId);
        model.addAttribute("outfit", outfit);
        model.addAttribute("backReferer", request.getHeader("referer"));
        model.addAttribute("clothList", clothService.getAllNoFilter());
        return "outfit-add";
    }

    @GetMapping("/delete/{outfitID}")
    public String deleteOutfit(HttpServletRequest request,
                               @PathVariable(name = "outfitID") Long outfitId) {
        outfitService.deleteById(outfitId);
        return "redirect:" + request.getHeader("referer");
    }

//    private void sendBrandsClothTypesOccasionsSeasons(Model model) {
//        model.addAttribute("brands", brandService.getAll());
//        model.addAttribute("clothTypes", clothTypeService.getAll());
//        model.addAttribute("occasionList", occasionService.findAll());
//        model.addAttribute("seasonList", seasonService.findAll());
//    }
//
//    private void sendOutfitBackRefererClothList(Model model, Long outfitId, HttpServletRequest request) {
//        Outfit outfit = outfitService.findById(outfitId);
//        model.addAttribute("outfit", outfit);
//        model.addAttribute("backReferer", request.getHeader("referer"));
//        model.addAttribute("clothList", clothService.getAllNoFilter());
//    }
}
