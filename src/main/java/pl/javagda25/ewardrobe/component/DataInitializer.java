package pl.javagda25.ewardrobe.component;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.javagda25.ewardrobe.model.Brand;
import pl.javagda25.ewardrobe.model.ClothType;
import pl.javagda25.ewardrobe.model.Occasion;
import pl.javagda25.ewardrobe.model.Season;
import pl.javagda25.ewardrobe.model.enums.BrandNames;
import pl.javagda25.ewardrobe.model.enums.ClothTypeNames;
import pl.javagda25.ewardrobe.model.enums.OccasionName;
import pl.javagda25.ewardrobe.model.enums.SeasonName;
import pl.javagda25.ewardrobe.repository.BrandRepository;
import pl.javagda25.ewardrobe.repository.ClothTypeRepository;
import pl.javagda25.ewardrobe.repository.OccasionRepository;
import pl.javagda25.ewardrobe.repository.SeasonRepository;

@AllArgsConstructor
@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {
    private final OccasionRepository occasionRepository;
    private final SeasonRepository seasonRepository;
    private final BrandRepository brandRepository;
    private final ClothTypeRepository clothTypeRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        for (ClothTypeNames clothTypename : ClothTypeNames.values()) {
            addDefaultClothTypeName(String.valueOf(clothTypename));
        }

        for (OccasionName occasionName : OccasionName.values()) {
            addDefaultOccasion(String.valueOf(occasionName));
        }

        for (SeasonName seasonName : SeasonName.values()) {
            addDefaultSeason(seasonName);
        }

        for (BrandNames brandName : BrandNames.values()) {
            addDefaultBrand(String.valueOf(brandName));
        }
    }

    private void addDefaultClothTypeName(String clothTypeName) {
        if (!clothTypeRepository.existsByName(clothTypeName)) {
            ClothType defaultClothType = new ClothType();
            defaultClothType.setName(clothTypeName);
            clothTypeRepository.save(defaultClothType);
        }
    }

    private void addDefaultSeason(SeasonName seasonName) {
        if (!seasonRepository.existsBySeasonName(seasonName)) {
            Season season = new Season();
            season.setSeasonName(seasonName);
            seasonRepository.save(season);
        }
    }

    private void addDefaultOccasion(String occasionName) {
        if (!occasionRepository.existsByOccasionName(occasionName)) {
            Occasion defaultOccasion = new Occasion();
            defaultOccasion.setOccasionName(occasionName);
            occasionRepository.save(defaultOccasion);
        }
    }

    private void addDefaultBrand(String brandName) {
        if (!brandRepository.existsByBrandName(brandName)) {
            Brand defaultBrand = new Brand();
            defaultBrand.setBrandName(brandName);
            brandRepository.save(defaultBrand);
        }
    }
}
