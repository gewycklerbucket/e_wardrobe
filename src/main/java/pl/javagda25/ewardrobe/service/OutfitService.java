package pl.javagda25.ewardrobe.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.javagda25.ewardrobe.model.Cloth;
import pl.javagda25.ewardrobe.model.ClothType;
import pl.javagda25.ewardrobe.model.Outfit;
import pl.javagda25.ewardrobe.repository.ClothRepository;
import pl.javagda25.ewardrobe.repository.OutfitRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;


@AllArgsConstructor
@Service
public class OutfitService {
    private final OutfitRepository outfitRepository;
    private final ClothRepository clothRepository;

    public Outfit save(Outfit outfit) {
        outfit.setTheSeason();
        outfit.setTheSeason();
        return outfitRepository.save(outfit);
    }

    public void addClothToOutfit(Long outfitId, Long clothId) {
        Optional<Outfit> optionalOutfit = outfitRepository.findById(outfitId);
        Optional<Cloth> optionalCloth = clothRepository.findById(clothId);

        if (optionalCloth.isPresent()) {
            Cloth newCloth = optionalCloth.get(); //TEN CHCEMY DODAĆ

            if (optionalOutfit.isPresent()) {
                Outfit outfit = optionalOutfit.get(); //DO TEGO CHCEMY DODAĆ NOWY CIUCH
                outfit.setName(optionalOutfit.get().getName());

                ClothType clTypeGiven = newCloth.getClothType(); //TYP CIUCHU KTÓRY TRZEBA SPRAWDZIĆ CZY ISTNIEJE W OUTFICIE

                if (!outfit.checkIfContains(clTypeGiven)) {
                    outfit.getClothSet().add(newCloth);
                    outfitRepository.save(outfit);
                } else {
                    Optional<Cloth> oldCloth = outfit.getClothOfGivenType(clTypeGiven);
                    if (oldCloth.isPresent()) {
                        outfit.getClothSet().remove(oldCloth.get());
                        outfit.getClothSet().add(newCloth);
                        outfitRepository.save(outfit);
                    }
                }
            }
        }
    }

    public Outfit findById(Long outfitId) {
        Optional<Outfit> optionalOutfit = outfitRepository.findById(outfitId);
        if (optionalOutfit.isPresent()) {
            return optionalOutfit.get();
        }
        throw new EntityNotFoundException("not found Outfit with, id " + outfitId);
    }

    public void deleteIfNull() {
        List<Outfit> outfitList = outfitRepository.findAll();
        for (Outfit outfit : outfitList) {
            if (outfit.getClothSet().isEmpty()) {
                outfitRepository.deleteById(outfit.getOutfitId());
            }
        }
    }

    public void removeClothFromOutfit(Long outfitId, Long clothId) {
        Optional<Outfit> optionalOutfit = outfitRepository.findById(outfitId);
        Optional<Cloth> optionalCloth = clothRepository.findById(clothId);

        optionalOutfit.ifPresent(o -> o.getClothSet().remove(optionalCloth.get()));
        outfitRepository.save(optionalOutfit.get());
    }

    public List<Outfit> findAll() {
        return outfitRepository.findAll();
    }

    public void deleteById(Long outfitId) {
        outfitRepository.deleteById(outfitId);
    }
}
