package pl.javagda25.ewardrobe.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.javagda25.ewardrobe.model.ClothType;
import pl.javagda25.ewardrobe.repository.ClothTypeRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class ClothTypeService {
    private final ClothTypeRepository clothTypeRepository;

    public ClothType addClothType(ClothType clothType) {
        clothType.setName(clothType.getName().toUpperCase());
        return clothTypeRepository.save(clothType);
    }

    public List<ClothType> getAll() {
        return clothTypeRepository.findAll();
    }
}
