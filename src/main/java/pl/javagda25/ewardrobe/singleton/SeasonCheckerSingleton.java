package pl.javagda25.ewardrobe.singleton;

public class SeasonCheckerSingleton {
    private static SeasonChecker seasonChecker;

    private SeasonCheckerSingleton() {
    }

    public static SeasonChecker getInstance() {
        if (null == seasonChecker) {
            seasonChecker = new SeasonChecker();
        }
        return seasonChecker;
    }
}
