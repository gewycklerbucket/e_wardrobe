package pl.javagda25.ewardrobe.singleton;

import pl.javagda25.ewardrobe.model.enums.SeasonName;

import java.time.LocalDate;

public class SeasonChecker {
    public Boolean inSeason(SeasonName seasonName) {
        int currentDay = LocalDate.now().getDayOfYear();
        if (currentDay <= seasonName.getEndDay() && currentDay >= seasonName.getStartDay()) {
            return true;
        }
        return false;

    }
}
